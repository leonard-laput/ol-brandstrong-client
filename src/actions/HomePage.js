import * as ActionTypes from '../constants/ActionTypes';

export function reset() {
	return async dispatch => {
		dispatch({
			type: ActionTypes.RESET_HOME_PAGE
		});
	};
}
