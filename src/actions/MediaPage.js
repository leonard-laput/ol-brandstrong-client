import gql from 'graphql-tag';
import * as ActionTypes from '../constants/ActionTypes';
import { client } from '../index';

export function onFieldChange(fieldName, fieldValue) {
	return async dispatch => {
		dispatch({
			type: ActionTypes.UPDATE_MEDIA_PAGE_FORM_FIELD,
			payload: {
				fieldName,
				fieldValue
			}
		});
	};
}

export function getMedia(id) {
	return async dispatch => {
		dispatch({
			type: ActionTypes.GET_MEDIA_REQUEST,
			payload: {
				loading: true
			}
		});

		const response = await client.query({
			query: gql`
				query media($id: ID!) {
					media(id: $id) {
						meta {
							code
							message
						}
						data {
							_id
							url
							user {
								_id
								name
								username
							}
						}
					}
				}
			`,
			variables: { id },
			fetchPolicy: 'no-cache'
		});
		const { meta, data } = response.data.media;

		if (meta.code !== 200) {
			dispatch({
				type: ActionTypes.GET_MEDIA_ERROR,
				payload: {
					loading: false,
					meta
				}
			});
			return;
		}

		dispatch({
			type: ActionTypes.GET_MEDIA_SUCCESS,
			payload: {
				loading: false,
				media: data
			}
		});
	};
}

export function getComments(id) {
	return async dispatch => {
		dispatch({
			type: ActionTypes.GET_MEDIA_COMMENTS_REQUEST,
			payload: {
				loadingComments: true
			}
		});

		const response = await client.query({
			query: gql`
				query comments($media: ID!) {
					comments(media: $media) {
						meta {
							code
							message
						}
						data {
							_id
							text
							user {
								_id
								name
								username
								avatar_url
							}
							created_at
						}
					}
				}
			`,
			variables: { media: id },
			fetchPolicy: 'no-cache'
		});
		const { meta, data } = response.data.comments;

		if (meta.code !== 200) {
			dispatch({
				type: ActionTypes.GET_MEDIA_COMMENTS_ERROR,
				payload: {
					loadingComments: false,
					meta
				}
			});
			return;
		}

		dispatch({
			type: ActionTypes.GET_MEDIA_COMMENTS_SUCCESS,
			payload: {
				loadingComments: false,
				comments: data
			}
		});
	};
}

export function addComment() {
	return async (dispatch, getState) => {
		const token = localStorage.getItem('tkn');
		const { text, media } = getState().mediaPageReducer;

		dispatch({
			type: ActionTypes.ADD_MEDIA_COMMENT_REQUEST,
			payload: {
				loadingSubmit: true,
				hasAddedComment: false
			}
		});

		const response = await client.mutate({
			mutation: gql`
				mutation addComment($text: String!, $asset: ID!, $token: String!) {
					addComment(text: $text, asset: $asset, token: $token) {
						meta {
							code
							message
						}
					}
				}
			`,
			variables: { text, asset: media._id, token }
		});
		const { meta } = response.data.addComment;

		if (meta.code !== 200) {
			dispatch({
				type: ActionTypes.ADD_MEDIA_COMMENT_ERROR,
				payload: {
					loadingSubmit: false,
					meta
				}
			});
			return;
		}

		dispatch({
			type: ActionTypes.ADD_MEDIA_COMMENT_SUCCESS,
			payload: {
				meta,
				loadingSubmit: false,
				hasAddedComment: true,
				text: ''
			}
		});
	};
}

export function resetMeta() {
	return async dispatch => {
		dispatch({
			type: ActionTypes.RESET_MEDIA_PAGE_META
		});
	};
}

export function reset() {
	return async dispatch => {
		dispatch({
			type: ActionTypes.RESET_MEDIA_PAGE
		});
	};
}
