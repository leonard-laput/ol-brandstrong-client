import gql from 'graphql-tag';
import * as ActionTypes from '../constants/ActionTypes';
import { client } from '../index';

export function onFieldChange(fieldName, fieldValue) {
	return async dispatch => {
		dispatch({
			type: ActionTypes.UPDATE_SIGN_UP_PAGE_FORM_FIELD,
			payload: {
				fieldName,
				fieldValue
			}
		});
	};
}

export function signup() {
	return async (dispatch, getState) => {
		const { name, email, username, password, confirm_password, country, state, city } = getState().signupPageReducer;

		dispatch({
			type: ActionTypes.SIGN_UP_REQUEST
		});

		if (password !== confirm_password) {
			dispatch({
				type: ActionTypes.SIGN_UP_ERROR,
				payload: {
					error: 'Passwords are not the same'
				}
			});
			return;
		}

		const response = await client.mutate({
			mutation: gql`
				mutation signup($name: String!, $email: String!, $username: String!, $password: String!, $country: String, $state: String, $city: String) {
					signup(name: $name, email: $email, username: $username, password: $password, country: $country, state: $state, city: $city) {
						meta {
							code
							message
						}
					}
				}
			`,
			variables: { name, email, username, password, country, state, city }
		});
		const { meta } = response.data.signup;

		if (meta.code !== 200) {
			dispatch({
				type: ActionTypes.SIGN_UP_ERROR,
				payload: {
					error: meta.message
				}
			});
			return;
		}

		dispatch({
			type: ActionTypes.SIGN_UP_SUCCESS,
			payload: {
				meta,
				done: true
			}
		});
	};
}

export function resetMeta() {
	return async dispatch => {
		dispatch({
			type: ActionTypes.RESET_SIGN_UP_PAGE_META
		});
	};
}

export function reset() {
	return async dispatch => {
		dispatch({
			type: ActionTypes.RESET_SIGN_UP_PAGE
		});
	};
}
