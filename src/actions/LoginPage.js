import gql from 'graphql-tag';
// import { push } from 'connected-react-router';
import moment from 'moment';
import * as ActionTypes from '../constants/ActionTypes';
import { client } from '../index';

export function onFieldChange(fieldName, fieldValue) {
	return async dispatch => {
		dispatch({
			type: ActionTypes.UPDATE_LOGIN_PAGE_FORM_FIELD,
			payload: {
				fieldName,
				fieldValue
			}
		});
	};
}

export function login(username, password) {
	return async dispatch => {
		dispatch({
			type: ActionTypes.LOGIN_REQUEST
		});
		console.log(client);
		const response = await client.query({
			query: gql`
				query login($username: String!, $password: String!) {
					login(username: $username, password: $password) {
						meta {
							code
							message
						}
						data {
							auth {
								token
							}
							user {
								_id
								username
								name
								avatar_url
							}
						}
					}
				}
			`,
			variables: { username, password }
		});
		const { meta, data } = response.data.login;

		if (meta.code !== 200) {
			dispatch({
				type: ActionTypes.LOGIN_ERROR,
				payload: {
					error: meta.message
				}
			});
			return;
		}

		dispatch({
			type: ActionTypes.LOGIN_SUCCESS,
			payload: {
				meta
			}
		});

		const user = data.user;
		user.id = data.user._id;

		// store response
		localStorage.setItem('tkn', data.auth.token);
		localStorage.setItem('user', JSON.stringify(user));
		localStorage.setItem('lastLogin', moment());

		// Navigate to home page
		// dispatch(push('/'));
		window.location = '/';
	};
}

export function resetMeta() {
	return async dispatch => {
		dispatch({
			type: ActionTypes.RESET_LOGIN_PAGE_META
		});
	};
}

export function reset() {
	return async dispatch => {
		dispatch({
			type: ActionTypes.RESET_LOGIN_PAGE
		});
	};
}
