/* eslint-disable no-undef */
import gql from 'graphql-tag';
import * as ActionTypes from '../constants/ActionTypes';
import { client } from '../index';

export function getMedias() {
	return async dispatch => {
		dispatch({
			type: ActionTypes.GET_APP_MEDIAS_REQUEST
		});

		const response = await client.query({
			query: gql`
				{
					medias {
						meta {
							code
							message
						}
						data {
							_id
							url
							user {
								_id
								name
								username
							}
						}
					}
				}
			`,
			fetchPolicy: 'no-cache'
		});
		const { meta, data } = response.data.medias;

		if (meta.code !== 200) {
			dispatch({
				type: ActionTypes.GET_APP_MEDIAS_ERROR,
				payload: {
					meta
				}
			});
			return;
		}

		dispatch({
			type: ActionTypes.GET_APP_MEDIAS_SUCCESS,
			payload: {
				medias: data
			}
		});
	};
}

export function addMedia(media) {
	return async dispatch => {
		const token = localStorage.getItem('tkn');
		const { file_name, file } = media;

		dispatch({
			type: ActionTypes.ADD_MEDIA_REQUEST,
			payload: {
				hasUploaded: false
			}
		});

		const response = await client.mutate({
			mutation: gql`
				mutation addMedia($file_name: String!, $file: String!, $is_media: Boolean, $token: String!) {
					addMedia(file_name: $file_name, file: $file, is_media: $is_media, token: $token) {
						meta {
							code
							message
						}
					}
				}
			`,
			variables: { file_name, file, is_media: true, token }
		});
		const { meta } = response.data.addMedia;

		if (meta.code !== 200) {
			dispatch({
				type: ActionTypes.ADD_MEDIA_ERROR,
				payload: {
					meta
				}
			});
			return;
		}

		dispatch({
			type: ActionTypes.ADD_MEDIA_SUCCESS,
			payload: {
				meta,
				hasUploaded: true
			}
		});
	};
}

export function logout() {
	return async dispatch => {
		const token = localStorage.getItem('tkn');

		const response = await client.query({
			query: gql`
				query logout($token: String!) {
					logout(token: $token) {
						meta {
							code
							message
						}
					}
				}
			`,
			variables: { token }
		});
		const { meta } = response.data.logout;

		if (meta.code !== 200) {
			dispatch({
				type: ActionTypes.LOGOUT_ERROR,
				payload: {
					error: meta.message
				}
			});
			return;
		}

		dispatch({
			type: ActionTypes.LOGOUT_SUCCESS,
			payload: {
				meta
			}
		});

		window.location = '/';
	};
}

export function resetMeta() {
	return async dispatch => {
		dispatch({
			type: ActionTypes.RESET_APP_META
		});
	};
}
