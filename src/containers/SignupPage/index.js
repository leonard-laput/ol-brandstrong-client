import { connect } from 'react-redux';
import SignupPage from './SignupPage';
import { onFieldChange, signup, resetMeta, reset } from '../../actions/SignupPage';

const mapsStateToProps = state => ({
	signupPageProps: state.signupPageReducer
});

const mapsDispatchToProps = dispatch => ({
	onFieldChange(fieldName, fieldValue) {
		dispatch(onFieldChange(fieldName, fieldValue));
	},
	signup() {
		dispatch(signup());
	},
	resetMeta() {
		dispatch(resetMeta());
	},
	reset() {
		dispatch(reset());
	}
});

export default connect(
	mapsStateToProps,
	mapsDispatchToProps
)(SignupPage);
