import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from './../../components/hoc/SnackBar';
import { Paper, FormControl, FormHelperText, TextField, Button, CircularProgress } from '@material-ui/core';

class LoginPage extends Component {
	componentWillUnmount() {
		this.props.reset();
	}

	componentDidUpdate(prevProps) {
		const { meta } = this.props.loginPageProps;
		const { meta: prev_meta } = prevProps.loginPageProps;

		if (meta && meta !== prev_meta) {
			const type = meta.code === 200 ? 'success' : 'error';
			this.props.snackbar.showMessage(meta.message, type);
			this.props.resetMeta();
		}
	}

	onFieldChange = e => {
		const name = e.target.name;
		let value = null;
		if (e.target.type === 'checkbox') {
			value = e.target.checked;
		} else if (e.target.type === 'number') {
			value = +e.target.value;
		} else {
			value = e.target.value;
		}
		this.props.onFieldChange(name, value);
	};

	onSubmit = e => {
		e.preventDefault();
		const { username, password } = this.props.loginPageProps;
		this.props.login(username, password);
	};

	render() {
		const { classes, loginPageProps } = this.props;
		const { username, password, loading, error } = loginPageProps;

		return (
			<div className={classes.root}>
				<Paper className={classes.paper} elevation={1}>
					<form onSubmit={this.onSubmit}>
						<FormControl margin="normal" required fullWidth>
							<TextField type="text" name="username" label="Username" value={username} onChange={this.onFieldChange} required autoFocus autoComplete="off" />
						</FormControl>
						<FormControl margin="normal" required fullWidth>
							<TextField type="password" name="password" label="Password" value={password} onChange={this.onFieldChange} required autoComplete="off" />
						</FormControl>
						{error && (
							<FormHelperText error className={classes.error}>
								{error}
							</FormHelperText>
						)}
						<Button type="submit" variant="contained" color="secondary" size="large" fullWidth disabled={loading} className={classes.submitButton}>
							{!loading && 'Login'}
							{loading && <CircularProgress size={24} color="secondary" />}
						</Button>
					</form>
				</Paper>
			</div>
		);
	}
}

LoginPage.propTypes = {
	loginPageProps: PropTypes.object.isRequired,
	onFieldChange: PropTypes.func.isRequired,
	login: PropTypes.func.isRequired,
	resetMeta: PropTypes.func.isRequired,
	reset: PropTypes.func.isRequired
};

const styles = theme => ({
	root: {
		marginTop: 56,
		[theme.breakpoints.up('sm')]: {
			marginTop: 64
		},
		position: 'relative',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
	paper: {
		...theme.mixins.gutters(),
		paddingTop: theme.spacing.unit * 2,
		paddingBottom: theme.spacing.unit * 2,
		margin: theme.spacing.unit * 2,
		textAlign: 'center',
		width: 500
	},
	error: {
		textAlign: 'center',
		marginTop: theme.spacing.unit * 2,
		marginBottom: theme.spacing.unit * 2
	},
	submitButton: {
		marginTop: theme.spacing.unit * 2,
		marginBottom: theme.spacing.unit * 2
	}
});

// Use redux's compose to use multiple
// HOC(Higher Order Component) wrappers
export default compose(
	withSnackbar(),
	withStyles(styles)
)(LoginPage);
