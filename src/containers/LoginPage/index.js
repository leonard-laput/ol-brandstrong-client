import { connect } from 'react-redux';
import LoginPage from './LoginPage';
import { onFieldChange, login, resetMeta, reset } from '../../actions/LoginPage';

const mapsStateToProps = state => ({
	loginPageProps: state.loginPageReducer
});

const mapsDispatchToProps = dispatch => ({
	onFieldChange(fieldName, fieldValue) {
		dispatch(onFieldChange(fieldName, fieldValue));
	},
	login(username, password) {
		dispatch(login(username, password));
	},
	resetMeta() {
		dispatch(resetMeta());
	},
	reset() {
		dispatch(reset());
	}
});

export default connect(
	mapsStateToProps,
	mapsDispatchToProps
)(LoginPage);
