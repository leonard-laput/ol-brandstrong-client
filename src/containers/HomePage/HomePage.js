import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { Paper, GridList, GridListTile, GridListTileBar } from '@material-ui/core';
import Loader from './../../components/Loader';

export class HomePage extends Component {
	componentWillUnmount() {
		this.props.reset();
	}

	render() {
		const { classes, appProps } = this.props;
		const { medias, loading } = appProps;

		return (
			<div className={classes.root}>
				{loading && <Loader />}
				{!loading && (
					<Paper className={classes.paper} elevation={1}>
						{medias.length === 0 && 'No medias available'}
						{medias.length > 0 && (
							<GridList cellHeight={180} className={classes.gridList}>
								{medias.map(media => (
									<GridListTile key={media._id} component={Link} to={`/media/${media._id}`}>
										<img src={media.url} alt="" />
										<GridListTileBar title={<span>by: {media.user.username}</span>} />
									</GridListTile>
								))}
							</GridList>
						)}
					</Paper>
				)}
			</div>
		);
	}
}

HomePage.propTypes = {
	appProps: PropTypes.object.isRequired,
	homePageProps: PropTypes.object.isRequired,
	reset: PropTypes.func.isRequired
};

const styles = theme => ({
	root: {
		marginTop: 56,
		[theme.breakpoints.up('sm')]: {
			marginTop: 64
		},
		position: 'relative',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
	paper: {
		...theme.mixins.gutters(),
		paddingTop: theme.spacing.unit * 2,
		paddingBottom: theme.spacing.unit * 2,
		margin: theme.spacing.unit * 2,
		textAlign: 'center',
		width: 500
	},
	gridList: {
		paddingTop: theme.spacing.unit,
		width: 500,
		height: 450
	}
});

// Use redux's compose to use multiple
// HOC(Higher Order Component) wrappers
export default compose(withStyles(styles))(HomePage);
