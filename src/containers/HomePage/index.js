import { connect } from 'react-redux';
import HomePage from './HomePage';
import { reset } from '../../actions/HomePage';

const mapsStateToProps = state => ({
	appProps: state.appReducer,
	homePageProps: state.homePageReducer
});

const mapsDispatchToProps = dispatch => ({
	reset() {
		dispatch(reset());
	}
});

export default connect(
	mapsStateToProps,
	mapsDispatchToProps
)(HomePage);
