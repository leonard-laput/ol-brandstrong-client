import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from './../../components/hoc/SnackBar';
import { Paper, Divider, Typography, List, ListItem, ListItemText, Avatar, TextField, Button, CircularProgress } from '@material-ui/core';
import Loader from './../../components/Loader';
import auth_helper from './../../helpers/auth_helper';

class MediaPage extends Component {
	componentWillUnmount() {
		this.props.reset();
	}

	componentDidUpdate(prevProps) {
		const { meta, hasAddedComment } = this.props.mediaPageProps;
		const { meta: prev_meta, hasAddedComment: prev_hasAddedComment } = prevProps.mediaPageProps;

		if (meta && meta !== prev_meta) {
			const type = meta.code === 200 ? 'success' : 'error';
			this.props.snackbar.showMessage(meta.message, type);
			this.props.resetMeta();
		}

		const id = this.props.match.params.id;
		if (hasAddedComment && hasAddedComment !== prev_hasAddedComment) {
			this.props.getComments(id);
		}
	}

	componentDidMount() {
		const id = this.props.match.params.id;
		this.props.getMedia(id);
		this.props.getComments(id);
	}

	onFieldChange = e => {
		const name = e.target.name;
		let value = null;
		if (e.target.type === 'checkbox') {
			value = e.target.checked;
		} else if (e.target.type === 'number') {
			value = +e.target.value;
		} else {
			value = e.target.value;
		}
		this.props.onFieldChange(name, value);
	};

	onSubmit = e => {
		e.preventDefault();
		const { text } = this.props.mediaPageProps;
		this.props.addComment(text);
	};

	render() {
		const { classes, mediaPageProps } = this.props;
		const { media, comments, text, loading, loadingComments, loadingSubmit } = mediaPageProps;

		return (
			<div className={classes.root}>
				{loading && <Loader />}
				{!loading && (
					<Paper className={classes.paper} elevation={1}>
						<img src={media.url} alt="" className={classes.image} />
						{!auth_helper.isAuthenticated() && (
							<React.Fragment>
								<Typography variant="h6" gutterBottom>
									Please login to comment
								</Typography>
								<Divider />
							</React.Fragment>
						)}
						{auth_helper.isAuthenticated() && (
							<form onSubmit={this.onSubmit} className={classes.form}>
								<TextField type="text" name="text" label="Comment" value={text} onChange={this.onFieldChange} required autoFocus autoComplete="off" margin="normal" fullWidth />
								<Button type="submit" variant="contained" color="secondary" disabled={loadingSubmit}>
									{!loadingSubmit && 'Submit'}
									{loadingSubmit && <CircularProgress size={24} color="secondary" />}
								</Button>
							</form>
						)}
						{loadingComments && (
							<Typography variant="h6" gutterBottom>
								Loading comments
							</Typography>
						)}
						{!loadingComments && (
							<React.Fragment>
								{comments.length === 0 && (
									<Typography variant="h6" gutterBottom>
										No comments available
									</Typography>
								)}
								{comments.length > 0 && (
									<List>
										{comments.map(comment => {
											const created_at = moment(comment.created_at).format('YYYY-MM-DD HH:mm');
											const secondary = `by ${comment.user.username} at ${created_at}`;
											return (
												<ListItem>
													<Avatar alt="profile pic" src={comment.user.avatar_url || '/avatar.jpg'} />
													<ListItemText primary={comment.text} secondary={secondary} />
												</ListItem>
											);
										})}
									</List>
								)}
							</React.Fragment>
						)}
					</Paper>
				)}
			</div>
		);
	}
}

MediaPage.propTypes = {
	mediaPageProps: PropTypes.object.isRequired,
	onFieldChange: PropTypes.func.isRequired,
	setMedia: PropTypes.func.isRequired,
	getMedia: PropTypes.func.isRequired,
	getComments: PropTypes.func.isRequired,
	addComment: PropTypes.func.isRequired,
	resetMeta: PropTypes.func.isRequired,
	reset: PropTypes.func.isRequired
};

const styles = theme => ({
	root: {
		marginTop: 56,
		[theme.breakpoints.up('sm')]: {
			marginTop: 64
		},
		position: 'relative',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
	paper: {
		...theme.mixins.gutters(),
		paddingTop: theme.spacing.unit * 2,
		paddingBottom: theme.spacing.unit * 2,
		margin: theme.spacing.unit * 2,
		textAlign: 'center',
		width: 500
	},
	image: {
		maxHeight: 150,
		[theme.breakpoints.up('sm')]: {
			maxHeight: 300
		}
	},
	form: {
		textAlign: 'left'
	}
});

// Use redux's compose to use multiple
// HOC(Higher Order Component) wrappers
export default compose(
	withSnackbar(),
	withStyles(styles)
)(MediaPage);
