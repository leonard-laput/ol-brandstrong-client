import { connect } from 'react-redux';
import MediaPage from './MediaPage';
import { onFieldChange, getMedia, getComments, addComment, resetMeta, reset } from '../../actions/MediaPage';

const mapsStateToProps = state => ({
	mediaPageProps: state.mediaPageReducer
});

const mapsDispatchToProps = dispatch => ({
	onFieldChange(fieldName, fieldValue) {
		dispatch(onFieldChange(fieldName, fieldValue));
	},
	getMedia(id) {
		dispatch(getMedia(id));
	},
	getComments(id) {
		dispatch(getComments(id));
	},
	addComment(text) {
		dispatch(addComment(text));
	},
	resetMeta() {
		dispatch(resetMeta());
	},
	reset() {
		dispatch(reset());
	}
});

export default connect(
	mapsStateToProps,
	mapsDispatchToProps
)(MediaPage);
