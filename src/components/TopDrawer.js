import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { Link, withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography, Button, Menu, IconButton, Avatar, MenuItem } from '@material-ui/core';
import auth_helper from './../helpers/auth_helper';

class TopDrawer extends Component {
	state = {
		anchorEl: null
	};

	onLogout = () => {
		const { onLogout } = this.props;
		this.handleClose();
		onLogout();
	};

	handleMenu = event => {
		this.setState({ anchorEl: event.currentTarget });
	};

	handleClose = () => {
		this.setState({ anchorEl: null });
	};

	render() {
		const { classes, location } = this.props;
		const { pathname } = location;
		const user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null;
		let appbarTitle = '';
		const { anchorEl } = this.state;
		const open = Boolean(anchorEl);

		if (/\/login/.test(pathname)) {
			appbarTitle = 'Login';
		} else if (/\/signup/.test(pathname)) {
			appbarTitle = 'Sign Up';
		} else if (/\/media\/\w+/.test(pathname)) {
			appbarTitle = 'Media';
		} else {
			appbarTitle = 'Code Challenge';
		}

		return (
			<div className={classes.root}>
				<AppBar position="sticky" className={classes.appBar}>
					<Toolbar>
						<Link to="/">
							<img src="/logo.png" alt="Brandstrong" className={classes.logo} />
						</Link>
						<Typography variant="h6" color="secondary" className={classes.headerText}>
							{appbarTitle}
						</Typography>
						{!auth_helper.isAuthenticated() && (
							<React.Fragment>
								<Button component={Link} to="/login">
									Login
								</Button>
								<Button component={Link} to="/signup">
									Sign Up
								</Button>
							</React.Fragment>
						)}
						{auth_helper.isAuthenticated() && (
							<React.Fragment>
								<Typography variant="subtitle2" color="secondary">
									{user.name}
								</Typography>
								<IconButton aria-owns={open ? 'menu-appbar' : undefined} aria-haspopup="true" onClick={this.handleMenu} color="inherit">
									<Avatar alt="profile pic" src={user.avatar_url || '/avatar.jpg'} />
								</IconButton>
								<Menu
									id="menu-appbar"
									anchorEl={anchorEl}
									anchorOrigin={{
										vertical: 'top',
										horizontal: 'right'
									}}
									transformOrigin={{
										vertical: 'top',
										horizontal: 'right'
									}}
									open={open}
									onClose={this.handleClose}
								>
									<MenuItem component={Link} to="/profile" onClick={this.handleClose}>
										Profile
									</MenuItem>
									<MenuItem onClick={this.onLogout}>Logout</MenuItem>
								</Menu>
							</React.Fragment>
						)}
					</Toolbar>
				</AppBar>
			</div>
		);
	}
}

const styles = theme => ({
	root: {
		position: 'fixed',
		width: 100 + '%',
		top: 0,
		left: 0,
		zIndex: 100
	},
	headerText: {
		flexGrow: 1,
		fontSize: '1.1rem'
	},
	appBar: {
		boxShadow: 'none'
	},
	logo: {
		height: '30px',
		marginRight: '1rem'
	}
});

TopDrawer.propTypes = {
	classes: PropTypes.object.isRequired,
	location: PropTypes.object.isRequired
};

// Use redux's compose to use multiple
// HOC(Higher Order Component) wrappers
export default compose(
	withRouter,
	withStyles(styles)
)(TopDrawer);
