import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// import Button from '@material-ui/core/Button';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import WarningIcon from '@material-ui/icons/Warning';
import { withStyles } from '@material-ui/core/styles';

const variantIcon = {
	success: CheckCircleIcon,
	warning: WarningIcon,
	error: ErrorIcon,
	info: InfoIcon
};

const styles = theme => ({
	success: {
		backgroundColor: green[600]
	},
	error: {
		backgroundColor: theme.palette.error.dark
	},
	info: {
		backgroundColor: theme.palette.primary.dark
	},
	warning: {
		backgroundColor: amber[700]
	},
	icon: {
		fontSize: 20
	},
	iconVariant: {
		opacity: 0.9,
		marginRight: theme.spacing.unit
	},
	message: {
		display: 'flex',
		alignItems: 'center'
	}
});

function MySnackbarContent(props) {
	const { classes, className, message, onClose, variant, ...other } = props;
	const Icon = variantIcon[variant];

	return (
		<SnackbarContent
			className={classNames(classes[variant], className)}
			aria-describedby="client-snackbar"
			message={
				<span id="client-snackbar" className={classes.message}>
					<Icon className={classNames(classes.icon, classes.iconVariant)} />
					{message}
				</span>
			}
			action={[
				<IconButton key="close" aria-label="Close" color="inherit" className={classes.close} onClick={onClose}>
					<CloseIcon className={classes.icon} />
				</IconButton>
			]}
			{...other}
		/>
	);
}

MySnackbarContent.propTypes = {
	classes: PropTypes.object.isRequired,
	className: PropTypes.string,
	message: PropTypes.node,
	onClose: PropTypes.func,
	variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired
};

const MySnackbarContentWrapper = withStyles(styles)(MySnackbarContent);

export default class SnackbarProvider extends PureComponent {
	state = {
		type: null,
		message: null,
		open: false
	};

	getChildContext() {
		return {
			snackbar: {
				showMessage: this.showMessage
			}
		};
	}

	// /**
	//  * Display a message with this snackbar.
	//  * @param {string} message message to display
	//  * @param {string} [action] label for the action button
	//  * @param {function} [handleAction] click handler for the action button
	//  * @public
	//  */
	// showMessage = (message, action, handleAction) => {
	// 	this.setState({ open: true, message, action, handleAction });
	// };

	/**
	 * Display a message with this snackbar.
	 * @param {string} message message to display
	 * @param {string} type type of message ['success', 'warning', 'error', 'info']
	 * @public
	 */
	showMessage = (message, type) => {
		this.setState({ open: true, message, type });
	};

	// handleActionClick = () => {
	// 	this.handleClose();
	// 	this.state.handleAction();
	// };

	handleClose = () => {
		// this.setState({ open: false, handleAction: null });
		this.setState({ open: false });
	};

	// render() {
	// 	const { action, message, open } = this.state;

	// 	const { children, SnackbarProps = {} } = this.props;

	// 	return (
	// 		<React.Fragment>
	// 			{children}
	// 			<Snackbar
	// 				{...SnackbarProps}
	// 				open={open}
	// 				message={message || ''}
	// 				action={
	// 					action != null && (
	// 						<Button color="secondary" size="small" onClick={this.handleActionClick}>
	// 							{action}
	// 						</Button>
	// 					)
	// 				}
	// 				onClose={this.handleClose}
	// 			/>
	// 		</React.Fragment>
	// 	);
	// }

	render() {
		const { message, type, open } = this.state;

		const { children, SnackbarProps = {} } = this.props;

		return (
			<React.Fragment>
				{children}
				{type && (
					<Snackbar {...SnackbarProps} open={open} onClose={this.handleClose}>
						<MySnackbarContentWrapper variant={type || 'info'} message={message || ''} onClose={this.handleClose} />
					</Snackbar>
				)}
				{!type && <Snackbar {...SnackbarProps} open={open} message={message || ''} onClose={this.handleClose} />}
			</React.Fragment>
		);
	}
}

SnackbarProvider.childContextTypes = {
	snackbar: PropTypes.shape({
		showMessage: PropTypes.func
	})
};

SnackbarProvider.propTypes = {
	/**
	 * The children that are wrapped by this provider.
	 */
	children: PropTypes.node,
	/**
	 * Props to pass through to the snackbar.
	 */
	SnackbarProps: PropTypes.object
};
