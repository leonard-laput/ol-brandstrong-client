import React, { Component } from 'react';
import { compose } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import { CircularProgress } from '@material-ui/core';

class Loader extends Component {
	render() {
		const { classes } = this.props;

		return (
			<div className={classes.loading}>
				<CircularProgress className={classes.progress} />
			</div>
		);
	}
}

const styles = theme => ({
	loading: {
		position: 'absolute',
		top: 0,
		width: '100%',
		display: 'flex',
		height: 100 + 'vh',
		alignItems: 'center',
		justifyContent: 'center'
	},
	progress: {
		margin: theme.spacing.unit * 2
	}
});

// Use redux's compose to use multiple
// HOC(Higher Order Component) wrappers
export default compose(withStyles(styles))(Loader);
