import React, { Component } from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import auth_helper from './helpers/auth_helper';

/* containers */
import HomePage from './containers/HomePage';
import LoginPage from './containers/LoginPage';
import SignupPage from './containers/SignupPage';
import MediaPage from './containers/MediaPage';

const routes = [
	{ path: '/', exact: true, isPrivate: false, Component: HomePage },
	{ path: '/login', exact: true, isPrivate: false, Component: LoginPage },
	{ path: '/signup', exact: true, isPrivate: false, Component: SignupPage },
	{ path: '/media/:id', exact: true, isPrivate: false, Component: MediaPage }
];

const PrivateRoute = ({ component: Component, ...rest }) => (
	<Route
		{...rest}
		render={props =>
			auth_helper.isAuthenticated() ? (
				<Component {...props} />
			) : (
				<Redirect
					to={{
						pathname: '/login',
						state: { from: props.location }
					}}
				/>
			)
		}
	/>
);

export class Routes extends Component {
	render() {
		const { location } = this.props;

		return (
			<Switch location={location}>
				{routes.map(({ path, exact, isPrivate, Component }, key) => {
					if (isPrivate) {
						return <PrivateRoute key={0} path={path} exact={exact} component={Component} />;
					} else {
						return <Route key={key} path={path} exact={exact} render={props => <Component {...props} />} />;
					}
				})}
			</Switch>
		);
	}
}

export default withRouter(Routes);
