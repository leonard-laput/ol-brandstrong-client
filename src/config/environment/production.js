export default {
	graphql: 'https://ol-brandstrong-graphql.herokuapp.com',
	loginTimeout: 86400
};
