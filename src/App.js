import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from './components/hoc/SnackBar';
import { DropzoneDialog } from 'material-ui-dropzone';
import TopDrawer from './components/TopDrawer';
import { Fab } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import { getMedias, addMedia, logout, resetMeta } from './actions/App';
import Routes from './routes';
import auth_helper from './helpers/auth_helper';
import file_helper from './helpers/file_helper';

class App extends Component {
	state = {
		open: false
	};

	componentDidMount() {
		this.props.getMedias();
	}

	componentDidUpdate(prevProps) {
		const { meta, hasUploaded } = this.props.appProps;
		const { meta: prev_meta, hasUploaded: prev_hasUploaded } = prevProps.appProps;
		if (meta && meta !== prev_meta) {
			const type = meta.code === 200 ? 'success' : 'error';
			this.props.snackbar.showMessage(meta.message, type);
			this.props.resetMeta();
		}

		if (hasUploaded && hasUploaded !== prev_hasUploaded) {
			this.setState({ open: false });
			this.props.getMedias();
		}
	}

	toggleDropzone = () => {
		this.setState(prevState => ({
			open: !prevState.open
		}));
	};

	handleOpen = () => this.setState({ open: true });

	handleClose = () => this.setState({ open: false });

	onUpload = files => {
		(async () => {
			if (!_.isEmpty(files)) {
				const file = files[0];
				const base64String = await file_helper.fileToDataUrl(file);
				const media = {
					file_name: file.name,
					file: base64String
				};
				this.props.addMedia(media);
			}
		})();
	};

	onLogout = () => {
		this.props.logout();
	};

	render() {
		const { classes } = this.props;

		return (
			<React.Fragment>
				<div className={classes.container}>
					<TopDrawer onLogout={this.onLogout} />
					<Routes />
					{auth_helper.isAuthenticated() && (
						<Fab color="secondary" aria-label="Add" className={classes.fab} onClick={this.handleOpen}>
							<Add />
						</Fab>
					)}
				</div>
				<DropzoneDialog
					open={this.state.open}
					onSave={this.onUpload}
					acceptedFiles={['image/jpeg', 'image/jpg', 'image/png']}
					maxFileSize={5000000}
					showAlerts={false}
					showPreviews={false}
					showPreviewsInDropzone={true}
					filesLimit={1}
					onClose={this.handleClose}
				/>
			</React.Fragment>
		);
	}
}

App.propTypes = {
	appProps: PropTypes.object.isRequired,
	getMedias: PropTypes.func.isRequired,
	addMedia: PropTypes.func.isRequired,
	logout: PropTypes.func.isRequired,
	resetMeta: PropTypes.func.isRequired
};

const styles = theme => ({
	container: {
		display: 'flex',
		flexDirection: 'column',
		height: 100 + 'vh'
	},
	fab: {
		position: 'fixed',
		bottom: theme.spacing.unit * 2,
		right: theme.spacing.unit * 2
	}
});

const mapsStateToProps = state => ({
	appProps: state.appReducer
});

const mapsDispatchToProps = dispatch => ({
	getMedias() {
		dispatch(getMedias());
	},
	addMedia(media) {
		dispatch(addMedia(media));
	},
	logout() {
		dispatch(logout());
	},
	resetMeta() {
		dispatch(resetMeta());
	}
});

// Use redux's compose to use multiple
// HOC(Higher Order Component) wrappers
export default compose(
	withSnackbar(),
	withStyles(styles),
	connect(
		mapsStateToProps,
		mapsDispatchToProps
	)
)(App);
