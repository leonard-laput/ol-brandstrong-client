import * as ActionTypes from '../constants/ActionTypes';

const initialState = {};

export default function homePageReducer(state = initialState, action) {
	switch (action.type) {
		case ActionTypes.RESET_HOME_PAGE:
			return {
				...initialState
			};
		default:
			return state;
	}
}
