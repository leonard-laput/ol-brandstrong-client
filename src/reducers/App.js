import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
	medias: [],
	loading: false,
	hasUploaded: false,
	validToken: false
};

export default function appReducer(state = initialState, action) {
	switch (action.type) {
		case ActionTypes.GET_APP_MEDIAS_REQUEST:
			return {
				...state,
				loading: true
			};
		case ActionTypes.GET_APP_MEDIAS_ERROR:
			return {
				...state,
				...action.payload,
				loading: false
			};
		case ActionTypes.GET_APP_MEDIAS_SUCCESS:
			return {
				...state,
				...action.payload,
				loading: false
			};
		case ActionTypes.ADD_MEDIA_REQUEST:
		case ActionTypes.ADD_MEDIA_ERROR:
		case ActionTypes.ADD_MEDIA_SUCCESS:
			return {
				...state,
				...action.payload
			};
		case ActionTypes.LOGOUT_ERROR:
			return {
				...state,
				...action.payload
			};
		case ActionTypes.LOGOUT_SUCCESS:
			localStorage.setItem('tkn', '');
			localStorage.setItem('user', '');
			localStorage.setItem('lastLogin', '');
			return {
				...state,
				...action.payload
			};
		case ActionTypes.RESET_APP_META:
			return {
				...state,
				meta: null
			};
		default:
			return state;
	}
}
