import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import App from './App';
import HomePage from './HomePage';
import LoginPage from './LoginPage';
import SignupPage from './SignupPage';
import MediaPage from './MediaPage';

export default history =>
	combineReducers({
		router: connectRouter(history),
		appReducer: App,
		homePageReducer: HomePage,
		loginPageReducer: LoginPage,
		signupPageReducer: SignupPage,
		mediaPageReducer: MediaPage
	});
