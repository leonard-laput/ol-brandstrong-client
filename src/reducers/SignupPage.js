import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
	name: '',
	email: '',
	username: '',
	password: '',
	confirm_password: '',
	country: '',
	state: '',
	city: '',
	loading: false,
	error: '',
	done: false
};

export default function signupPageReducer(state = initialState, action) {
	switch (action.type) {
		case ActionTypes.UPDATE_SIGN_UP_PAGE_FORM_FIELD:
			return {
				...state,
				[action.payload.fieldName]: action.payload.fieldValue
			};
		case ActionTypes.SIGN_UP_REQUEST:
			return {
				...state,
				loading: true,
				error: ''
			};
		case ActionTypes.SIGN_UP_SUCCESS:
			return {
				...initialState,
				...action.payload
			};
		case ActionTypes.SIGN_UP_ERROR:
			return {
				...state,
				error: action.payload.error,
				password: '',
				confirm_password: '',
				loading: false
			};
		case ActionTypes.RESET_SIGN_UP_PAGE_META:
			return {
				...state,
				meta: null
			};
		case ActionTypes.RESET_SIGN_UP_PAGE:
			return {
				...initialState
			};
		default:
			return state;
	}
}
