import * as ActionTypes from '../constants/ActionTypes';

const initialState = {
	media: null,
	comments: [],
	text: '',
	loading: true,
	loadingComments: false,
	loadingSubmit: false,
	hasAddedComment: false
};

export default function mediaPageReducer(state = initialState, action) {
	switch (action.type) {
		case ActionTypes.UPDATE_MEDIA_PAGE_FORM_FIELD:
			return {
				...state,
				[action.payload.fieldName]: action.payload.fieldValue
			};
		case ActionTypes.GET_MEDIA_REQUEST:
		case ActionTypes.GET_MEDIA_ERROR:
		case ActionTypes.GET_MEDIA_SUCCESS:
		case ActionTypes.GET_MEDIA_COMMENTS_REQUEST:
		case ActionTypes.GET_MEDIA_COMMENTS_ERROR:
		case ActionTypes.GET_MEDIA_COMMENTS_SUCCESS:
		case ActionTypes.ADD_MEDIA_COMMENT_REQUEST:
		case ActionTypes.ADD_MEDIA_COMMENT_ERROR:
		case ActionTypes.ADD_MEDIA_COMMENT_SUCCESS:
			return {
				...state,
				...action.payload
			};
		case ActionTypes.RESET_MEDIA_PAGE_META:
			return {
				...state,
				meta: null
			};
		case ActionTypes.RESET_MEDIA_PAGE:
			return {
				...initialState
			};
		default:
			return state;
	}
}
