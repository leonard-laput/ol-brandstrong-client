import moment from 'moment';
import config from './../config';

const authHelper = {
	isAuthenticated() {
		let token = localStorage.getItem('tkn');
		let lastLogin = localStorage.getItem('lastLogin');
		let now = moment();

		if (token && lastLogin) {
			let tokenLifeInSeconds = now.diff(lastLogin, 'seconds');

			return tokenLifeInSeconds < config.loginTimeout;
		}

		return false;
	}
};

export default authHelper;
