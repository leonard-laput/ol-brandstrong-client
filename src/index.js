import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { SnackbarProvider } from './components/hoc/SnackBar';
import store, { history } from './store';
import App from './App';
import * as serviceWorker from './serviceWorker';
import config from './config';
import 'typeface-roboto';
import './index.scss';

export const client = new ApolloClient({
	uri: config.graphql
});

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#fcc433',
			contrastText: '#ffffff'
		},
		secondary: {
			main: '#0c1229',
			contrastText: '#ffffff'
		}
	},
	typography: { useNextVariants: true }
});

const SnackbarProps = {
	anchorOrigin: {
		vertical: 'bottom',
		horizontal: 'right'
	},
	autoHideDuration: 5000
};

ReactDOM.render(
	<ApolloProvider client={client}>
		<MuiThemeProvider theme={theme}>
			<Provider store={store()}>
				<SnackbarProvider SnackbarProps={SnackbarProps}>
					<ConnectedRouter history={history}>
						<App />
					</ConnectedRouter>
				</SnackbarProvider>
			</Provider>
		</MuiThemeProvider>
	</ApolloProvider>,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
